# Purpose

This repo is a collection of dockerfiles I use to build stuff. 

### go-appengine-builder 

A builder image I use as base-image for building appengine-go-projects on gitlab/ci. It is built and uploaded to dockerhub as nilslarsgard/go-appengine-builder. Latest version is 204.0.0

### go-elm-appengine-builder 

based on 'go-appengine-builder', but includes nodejs and elm for building elm-apps
